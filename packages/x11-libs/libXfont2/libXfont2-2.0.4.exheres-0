# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg

SUMMARY="Core of the legacy X11 font system"
DESCRIPTION="
libXfont provides the core of the legacy X11 font system, handling the
index files (fonts.dir, fonts.alias, fonts.scale), the various font file
formats, and rasterizing them.   It is used by the X servers, the
X Font Server (xfs), and some font utilities (bdftopcf for instance),
but should not be used by normal X11 clients.  X11 clients access fonts
via either the new API's in libXft, or the legacy API's in libX11.
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-libs/libbsd [[ note = [ automagic, but should prefer strlcat, strlcpy and reallocarray ] ]]
        x11-libs/xtrans
        x11-proto/xorgproto
        doc? ( app-text/xmlto[>=0.0.22] )
    build+run:
        media-libs/freetype:2
        x11-libs/libfontenc
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ipv6
    --disable-selective-werror # fails to build otherwise, last checked: 2.0.4
    --disable-static
    --with-bzip2
    --without-fop
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc devel-docs' )

