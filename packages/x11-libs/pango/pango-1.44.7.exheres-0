# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2008-2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="A library for layout and rendering of text"
HOMEPAGE="https://www.pango.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
"

RESTRICT="test"

# TODO(?) automagic dependency on libthai (not packaged)
DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gtk-doc? (
            app-text/docbook-xml-dtd:4.1.2
            dev-doc/gtk-doc[>=1.15]
        )
    build+run:
        dev-libs/fribidi[>=0.19.7]
        dev-libs/glib:2[>=2.59.2]
        media-libs/fontconfig[>=2.11.91]
        media-libs/freetype:2[>=2.1.5]
        x11-libs/cairo[>=1.12.10]
        x11-libs/harfbuzz[>=2.0.0]
        (
            x11-libs/libXft[>=2.0.0]
            x11-libs/libXrender
        ) [[ description = [ Xft backend is automagic ] ]]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
    test:
        fonts/cantarell-fonts
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dinstall-tests=false
    -Duse_fontconfig=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
)

src_prepare() {
    meson_src_prepare
    edo sed -e '/implicit-fallthrough/d' -i meson.build
}

