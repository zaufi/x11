# Copyright 2017-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=silnrsi project=graphite release=${PV} suffix=tgz ] \
    cmake

SUMMARY="Library providing rendering capabilities for complex non-Roman writing systems"
DESCRIPTION="
Graphite is a system that can be used to create smart fonts capable of displaying writing systems
with various complex behaviors. A smart font contains not only letter shapes but also additional
instructions indicating how to combine and position the letters in complex ways.
"
HOMEPAGE+=" http://graphite.sil.org"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    test:
        media-libs/freetype:2
"


# tests would create a dependency cycle between freetype:2 and harfbuzz
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DPYTHON_EXECUTABLE:PATH=""
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DCMAKE_DISABLE_FIND_PACKAGE_Freetype:BOOL=FALSE -DCMAKE_DISABLE_FIND_PACKAGE_Freetype:BOOL=TRUE'
)

src_prepare() {
    cmake_src_prepare

    # TODO: report upstream: !!! Exheres bug: 'readelf' banned by distribution
    edo sed \
        -e "s/readelf/$(exhost --target)-readelf/" \
        -i Graphite.cmake

    # disable failing tests due to missing fonttools dependency
    edo sed \
        -e '/cmptest/d' \
        -i tests/CMakeLists.txt

    # TODO: report upstream: fix installation location of .cmake files
    edo sed \
        -e "s/share/\${CMAKE_INSTALL_DATAROOTDIR}/" \
        -i src/CMakeLists.txt
}

